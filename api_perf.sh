#!/bin/bash

rm xx*
truncate -s 0 debug.log

cd ~/wikimedia/src/pywikibot/core/
python3 pwb.py all_params_api -m "query+allusers" -s 1 -r 1 -lang:en -family:ca1enwiki
cd -

csplit -n 5 debug.log /Start\ request\ POST\ \/w\/api.php/ {*}
grep -e "\[rdbms\] .*::execute\|^PARAMS:" xx* > grep.txt
python3 group.py
