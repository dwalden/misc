#!/usr/bin/python3

# Based on code in https://www.mediawiki.org/wiki/API:Delete.

# MIT license

import requests
import pprint
import json

from time import time
from multiprocessing import Pool

# URL = "https://en.wikipedia.beta.wmflabs.org/w/api.php"
URL = "https://commons.wikimedia.beta.wmflabs.org/w/api.php"
# URL = "https://test.wikipedia.org/w/api.php"
# URL = "http://localhost:8081/w/api.php"
# URL = "http://localhost/bare/api.php"

def login(session, username, password):

    # Step1: Retrieve login token
    PARAMS_0 = {
        'action':"query",
        'meta':"tokens",
        'type':"login",
        'format':"json"
    }

    R = session.get(url=URL, params=PARAMS_0)
    DATA = R.json()

    LOGIN_TOKEN = DATA['query']['tokens']['logintoken']

    # Step2: Send a post request to login. Use of main account for login is not
    # supported. Obtain credentials via Special:BotPasswords
    # (https://www.mediawiki.org/wiki/Special:BotPasswords) for lgname & lgpassword
    PARAMS_1 = {
        'action':"login",
        'lgname': username,
        'lgpassword': password,
        'lgtoken':LOGIN_TOKEN,
        'format':"json"
    }

    R = session.post(URL, data=PARAMS_1)

    # Step 3: When logged in, retrieve a CSRF token
    PARAMS_2 = {
        'action':"query",
        'meta':"tokens",
        'format':"json"
    }

    R = session.get(url=URL, params=PARAMS_2)
    DATA = R.json()

    return DATA['query']['tokens']['csrftoken']

def logout(session, token):
    # Step 4: Send a POST request to logout
    PARAMS_3 = {
        "action": "logout",
        "token": token,
        "format": "json"
    }
    R = session.post(URL, data=PARAMS_3)

def undelete(session, token, page):
    # Step 4: Send a post request to undelete a page
    PARAMS_3 = {
        'action':"undelete",
        'title': page,
        'undeletetalk': True,
        'reason': "Concurrency testing for T290210",
        'token': token,
        'format':"json"
    }
    start = time()
    try:
        response = session.post(URL, data=PARAMS_3)
        DATA = response.json()
    except json.decoder.JSONDecodeError as err:
        DATA = response.text
    except requests.exceptions.RequestException as err:
        DATA = err
    end = time()
    return page, start, end, DATA


pp = pprint.PrettyPrinter()

session1 = requests.Session()
session2 = requests.Session()

# The two bots who will be trying to delete the page at the same time.
token1 = login(session1, "<username>@<botname>", "<botpassword>")
token2 = login(session2, "<username>@<botname>", "<botpassword>")

for page in ["File:Test_file_for_T290210.png"]:

    iterable = [
        (session1, token1, page),
        (session2, token2, page)
    ]

    with Pool(2) as p:
        pp.pprint(p.starmap(undelete, iterable))

logout(session1, token1)
logout(session2, token2)
