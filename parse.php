<?php
/**
 * Copyright (C) 2023 Dominic Walden
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @ingroup Maintenance
 */

if ( getenv( 'MW_INSTALL_PATH' ) ) {
	$IP = getenv( 'MW_INSTALL_PATH' );
} else {
	$IP = __DIR__ . '/../../..';
}
require_once "$IP/maintenance/Maintenance.php";

use MediaWiki\Extension\Math\TexVC\TexVC;

$PACKAGES = ['ams', 'cancel', 'color', 'euro', 'teubner', 'mathoid'];

class ParseTex extends Maintenance {

	private $texVC;

	public function __construct() {
		parent::__construct();
		global $PACKAGES;

		$this->addDescription( <<<EOT
Parse a LaTeX command.
EOT
		);

		$this->addArg('input', 'TeX input to be parsed');
		# $this->addOption('rebuild', 'Rebuild PEGjs grammar before parsing');
		$this->addOption('verbose', 'Show verbose error information');
		$this->addOption('usemathrm', 'Use \\mathrm instead of \\mbox to escape some text literals');
		$this->addOption('usemhchem', 'Allow commands from the mhchem package');
		$this->addOption('debug', 'Show stack trace on failure');
		$this->addOption('info', 'use the info functionalities whih print the identified texvc ' .
						 'tokens identified by and further info');
		$this->addOption('compact', 'info: Do not pretty print output.');
		$this->addOption('flat', 'info: Flattens the tree for elements with only one child');
		$this->addOption('output', 'info: Output the info in a specific format. Available options are:\n' .
						 '"list": prints all tokens as list\n' .
						 '"tree": prints the texvc AST\n' .
						 '"json": a json object that can be visualized using d3\n' .
						 '"identifier": prints TeX code for all identifiers\n' .
						 '"all": is a combination of list, tree and identifier \n' .
						 '"feedback": returns data to generate user feedback in a ui', false, true);

		foreach ( $PACKAGES as $pkg ) {
			$msg = 'Fail validation if input requires the ';
			if ( $pkg === 'ams' ) {
				$msg .= 'ams* packages';
			} else {
				$msg .= $pkg . ' package';
			}
			$this->addOption( 'no-' . $pkg, $msg );
		};

		$this->requireExtension( 'Math' );
		$this->texVC = new TexVC();
	}

	public function execute() {
		// if ( $this->hasOption( 'info' ) ) {
		// 	if ( $this->getOption( 'output' ) === 'feedback' ) {
		// 		$result = texvcjs.feedback(input);
		// 	} else {
		// 		$result = texvcjs.texvcinfo(input, {
		// 				debug: options.debug,
		// 			   format: options.output,
		// 			   // eslint-disable-next-line es-x/no-array-prototype-flat
		// 			   flatTree: options.flat,
		// 			   compact: options.compact });
		// 	}

		// 	// output result
		// 	if (result.status === undefined) {
		// 		if (options.compact) {
		// 			console.log(JSON.stringify(result));
		// 		} else {
		// 			console.log(JSON.stringify(result, null, 2));
		// 		}
		// 	} else if (result.status === 'F' || options.verbose) {
		// 		console.log(result.status . (result.details || ''));
		// 	} else {
		// 		console.log(result.status . (result.details || ''));
		// 	}
		// 	// eslint-disable-next-line no-process-exit
		// 	process.exit(result.status === undefined ? 0 : 1);
		// }

		$result = $this->texvc->check( $input, [
			'debug' => $this->getOptions( 'debug' ),
			'usemathrm' => $this->getOptions( 'usemathrm' ),
			'usemhchem' => $this->getOptions( 'usemhchem' ) ] );

		// check required packages
		foreach ( $PACKAGES as $pkg ) {
			if ( $result[$pkg . '_required'] && !$this->hasOption( 'no-' . $pkg ) ) {
				$result['status'] = 'F';
				$result['details'] = $result[$pkg . '_required'];
			}
		}

		// output result
		if ( $result['status'] === '+' ) {
			$this->output( $result['status'] . ($result['output'] || '') );
		} else if ( $result['status'] === 'F' || $this->hasOption( 'verbose' ) ) {
			$this->output( $result['status'] . ($result['details'] || '') );
		} else {
			$this->output($result['status']);
		}
		// eslint-disable-next-line no-process-exit
		# process.exit(result.status === '+' ? 0 : 1);
	}
}

$maintClass = ParseTex::class;
require_once RUN_MAINTENANCE_IF_MAIN;
