#!/usr/bin/python3

# Based on code in https://www.mediawiki.org/wiki/API:Delete.

# MIT license

import requests
import pprint

from time import time
from multiprocessing import Pool

# URL = "https://en.wikipedia.beta.wmflabs.org/w/api.php"
URL = "https://test.wikipedia.org/w/api.php"

def login(session, username, password):

    # Step1: Retrieve login token
    PARAMS_0 = {
        'action':"query",
        'meta':"tokens",
        'type':"login",
        'format':"json"
    }

    R = session.get(url=URL, params=PARAMS_0)
    DATA = R.json()

    LOGIN_TOKEN = DATA['query']['tokens']['logintoken']

    # Step2: Send a post request to login. Use of main account for login is not
    # supported. Obtain credentials via Special:BotPasswords
    # (https://www.mediawiki.org/wiki/Special:BotPasswords) for lgname & lgpassword
    PARAMS_1 = {
        'action':"login",
        'lgname': username,
        'lgpassword': password,
        'lgtoken':LOGIN_TOKEN,
        'format':"json"
    }

    R = session.post(URL, data=PARAMS_1)

    # Step 3: When logged in, retrieve a CSRF token
    PARAMS_2 = {
        'action':"query",
        'meta':"tokens",
        'format':"json"
    }

    R = session.get(url=URL, params=PARAMS_2)
    DATA = R.json()

    return DATA['query']['tokens']['csrftoken']

def logout(session, token):
    # Step 4: Send a POST request to logout
    PARAMS_3 = {
        "action": "logout",
        "token": token,
        "format": "json"
    }
    R = session.post(URL, data=PARAMS_3)

def delete(session, token, page):
    # Step 4: Send a post request to delete a page
    PARAMS_3 = {
        'action':"delete",
        'title': page,
        'deletetalk': True,
        'reason': "Concurrency testing for T263209",
        'token': token,
        'format':"json"
    }
    start = time()
    response = session.post(URL, data=PARAMS_3)
    end = time()
    DATA = response.json()
    return page, start, end, DATA


pp = pprint.PrettyPrinter()

session1 = requests.Session()
session2 = requests.Session()

# The two bots who will be trying to delete the page at the same time.
token1 = login(session1, "<username>@<botname>", "<botpassword>")
token2 = login(session2, "<username>@<botname>", "<botpassword>")

# Pages you want to delete
for page in ["File:SD0001test", "File:Amory_St.jpg", "File:Jaania3.gif", "File:WpCaptchaId_734931244.gif", "File:Adobe_Photoshop_CS6_icon.svg", "File:Wiki.png", "File:Antiga_Clínica_Vilanova_(Andorra_la_Vella)_2012-09-04_16-34-44.jpg", "File:City_of_Paris_Building_2012-09-13_14-08-57.jpg", "File:Example222.png", "File:New_Orlands_Klasse_bei_Wolf_359.svg", "File:SD0001test.png", "File:Saint-Vallier-gare.jpg", "File:SystemScript.svg", "File:TestWikiLogo.PNG"]:

    iterable = [
        (session1, token1, page),
        (session2, token2, page)
    ]

    with Pool(2) as p:
        pp.pprint(p.starmap(delete, iterable))

logout(session1, token1)
logout(session2, token2)
