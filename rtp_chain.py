# Copyright (C) 2023 Dominic Walden

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
# http://www.gnu.org/copyleft/gpl.html

import random

n = 5

modes = ["regular", "manual"]
responses = ["success", "timeout", "no connection"]
methods = ["open", "already opened"]
weights = [50,     50]

mode = "regular"

for i in list(range(n)):
    if i >= 3:
        mode = random.choice(modes)
        if mode == "manual":
            modes = ["manual"]
    if i > 0:
        if mode != "manual":
            methods = ["automatic", "button", "shortcut", "close-reopen", "close-reopen with shortcut"]
            weights = [50,          10,       10,         10,             10]
        else:
            methods = ["button", "shortcut", "close-reopen", "close-reopen with shortcut"]
            weights = [25,       25,         25,             25]
    print("{} {} {}".format(mode, random.choice(responses), random.choices(methods, weights=weights, k=1)[0]))
