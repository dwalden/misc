$interfaces = [];
foreach ( $wgAutoloadLocalClasses as $className => $filePath ) {
	$ref = new ReflectionClass( $className );
	foreach( $ref->getInterfaceNames() as $interfaceName ) {
		$interfaces[$interfaceName][] = $className;
	}
}

function randomFromType( $type, $interfaces ) {
	if ( in_array( $type, ['int', 'array'] ) ) {
		if ( $type == 'int' ) {
			return 1;
		}
		if ( $type == 'array' ) {
			return [];
		}
	} else {
		$ref = new ReflectionClass( $type );
		if ( $ref->isInterface() ) {
			$randInt = array_rand( $interfaces[$ref->getName()] );
			$ref = new ReflectionClass( $interfaces[$ref->getName()][$randInt] );
		}
		$constructor = $ref->getConstructor();
		$array = [];
		if ( $constructor ) {
			foreach ( $constructor->getParameters() as $param ) {
				if ( $param->isOptional() ) {
					$array[] = $param->getDefaultValue();
				} else if ( $param->getType() ) {
					$array[] = randomFromType( $param->getType()->getName(), $interfaces );
				} else {
					$array[] = []; // anything();
				}
			}
		}
		print( $ref->getName() );
		var_dump( $array );
		return $ref->newInstanceArgs( $array );
	}
}

// $class = randomFromType( 'MediaWiki\Pager\BlockListPager', $interfaces );
// $class = randomFromType( 'ContextSource', $interfaces );
$class = randomFromType( 'IContextSource', $interfaces );
// $class = randomFromType( 'MediaWiki', $interfaces );
// $class = randomFromType( 'DerivativeContext', $interfaces );
