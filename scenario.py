# Copyright (C) 2023 Dominic Walden

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
# http://www.gnu.org/copyleft/gpl.html

import argparse
import random
import re
import subprocess

from datetime import datetime
import networkx as nx

parser = argparse.ArgumentParser()
parser.add_argument('-e', '--num_edges', type=int, default=10)
parser.add_argument('-s', '--num_scenarios', type=int, default=1)
options = parser.parse_args()

def relation(graph, name, relations, trigger = False, disaster = False, symmetric = True):
    for rel in relations:
        if type(rel) is str:
            graph.add_edge(rel, rel, label=name, color="black" if not trigger else "green")
        elif type(rel) is list:
            graph.add_edge(rel[0], rel[1], label=name, color="black" if not trigger else "green")
            if symmetric:
                graph.add_edge(rel[1], rel[0], label=name, color="black" if not trigger else "green")
        else:
            print("Invalid type of relation {}".format(rel), file=sys.stderr)

G = nx.MultiDiGraph()

# Nodes
G.add_node("view")
G.add_node("edit")
G.add_node("undo")
G.add_node("closed tab")
G.add_node("closed browser")
G.add_node("preview")
G.add_node("changes")

# Relations
relation(G, "refresh", ["view", "undo"], False, True, False)
relation(G, "refresh", ["edit", "preview", "changes"], True, True, False)
relation(G, "edit", ["edit", ["view", "edit"], "preview", "changes"], True, True)
relation(G, "edit", ["undo"], False, True, False)
relation(G, "same tab", [["view", "undo"], ["undo", "edit"]], False, True, False)
relation(G, "same tab", [["edit", "undo"]], True, True, False)
relation(G, "new tab", ["view", "undo", ["view", "undo"], ["undo", "edit"], ["closed tab", "view"], ["closed tab", "edit"], ["closed tab", "undo"], ["closed browser", "view"], ["closed browser", "edit"], ["closed browser", "undo"], ["closed tab", "preview"], ["closed tab", "changes"], ["closed browser", "preview"], ["closed browser", "changes"]], False, True, False)
relation(G, "new tab", ["edit", ["edit", "undo"]], True, True, False)
relation(G, "close", [["view", "closed tab"], ["view", "closed browser"], ["undo", "closed tab"], ["undo", "closed browser"], ["preview", "closed tab"], ["preview", "closed browser"], ["changes", "closed tab"], ["changes", "closed browser"]], False, True, False)
relation(G, "close", [["edit", "closed tab"], ["edit", "closed browser"]], True, True, False)
relation(G, "show preview", ["preview", ["view", "preview"], ["edit", "preview"], ["undo", "preview"], ["preview", "changes"]], True, True, True)
relation(G, "show changes", ["changes", ["view", "changes"], ["edit", "changes"], ["undo", "changes"], ["changes", "preview"]], True, True, True)

timestamp = datetime.now().strftime("%Y-%m-%dT%H:%M:%S")

base_filename = "complete_{}_{}".format(options.num_edges, timestamp)
dot_filename = "{}.dot".format(base_filename)
png_filename = "{}.png".format(base_filename)
nx.nx_agraph.write_dot(G, dot_filename)
with open(png_filename, "w") as png_file:
    subprocess.run(["dot", "-Tpng", dot_filename], stdout=png_file)

for n in range(0, options.num_scenarios):
    H = nx.MultiDiGraph()
    first_node = random.choice(list(nx.nodes(G)))
    H.add_node(first_node, color = "blue")
    for i in range(0, options.num_edges):
        edges = G.edges(first_node, data=True)
        if edges:
            edge = random.choice(list(edges))
            if not re.search('[0-9]+', edge[2]['label']):
                edge[2]['label'] += " {}".format(i)
            else:
                edge[2]['label'] = re.sub('[0-9]+', str(i), edge[2]['label'])
            H.add_node(edge[0])
            H.add_node(edge[1])
            H.add_edges_from([edge])
            first_node=edge[1]

    base_filename = "scenario_{}_{}_{}".format(timestamp, options.num_edges, n)
    dot_filename = "{}.dot".format(base_filename)
    png_filename = "{}.png".format(base_filename)
    nx.nx_agraph.write_dot(H, dot_filename)
    with open(png_filename, "w") as png_file:
        subprocess.run(["dot", "-Tpng", dot_filename], stdout=png_file)
