import json
import re
# import csv
import pandas

stats = {}
fields = []

with open("grep.txt", "r") as grep:
    for line in grep:
        identifier = re.findall(r'xx([0-9]+):', line)[0]
        if identifier not in stats:
            stats[identifier] = {}
        if 'PARAMS' in line:
            stats[identifier].update(json.loads(line[16:].replace("'", '"')))
        elif '::execute' in line:
            if 'time' not in stats[identifier]:
                stats[identifier]['time'] = 0
            stats[identifier]['time'] += float(re.findall(r'\[rdbms\] .*::execute \[([0-9.]+)ms\]', line)[0])
            # stats[identifier]['query'] = re.findall(r'\[rdbms\] ApiQueryBlocks::execute \[[0-9.]+ms\] mariadb-main: (.*)', line)[0]
        fields = list(set(fields + list(stats[identifier].keys())))

df = pandas.DataFrame.from_dict(list(stats.values()))
# print(df.groupby('bkusers').agg({'time': ['min', 'median', 'mean', 'max']}))
print(df.agg({'time': ['min', 'median', 'mean', lambda x: x.quantile(0.75), 'max']}))

# with open("group.csv", "a") as csvfile:
#     dictcsv = csv.DictWriter(csvfile, fields, restval=None, extrasaction="ignore")
#     dictcsv.writeheader()
#     dictcsv.writerows(list(stats.values()))
