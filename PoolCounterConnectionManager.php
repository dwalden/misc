use Eris\Generator;

$eris = new Eris\Facade();

$ref = new ReflectionClass( 'MediaWiki\PoolCounter\PoolCounterConnectionManager' );
$conn = $ref->newInstanceWithoutConstructor();

$gens = [];
foreach ( $ref->getProperties() as $property ) {
	$name = $property->getName();
	$comment = $property->getDocComment();
	preg_match( '/\@var ([a-z]*)(\[\])?/', $comment, $matches );
	switch( $matches[1] ) {
	case 'int':
		$type = Generator\int();
	case 'float':
		$type = Generator\float();
	case 'bool':
		$type = Generator\bool();
	case 'string':
		$type = Generator\string();
	case 'array':
		$type = Generator\seq( Generator\string() );
	default:
		$type = Generator\string();
	}
	if ( isset( $matches[2] ) ) {
		$type = Generator\seq( $type );
	}
	$gens[$name] = $type;
}

$count = [];

$eris
	->minimumEvaluationRatio(0)
	->forAll( Generator\associative( $gens ) )
	->then( function( $args ) use ( $ref, $conn, &$count ) {
		foreach ( $args as $name => $property ) {
			$conn->{$name} = $property;
		}
		$status = $conn->get( 'key' );
		$errors = $status->getErrors();
		foreach ( $errors as $error ) {
			if ( ! isset( $count[$error['message']][$error['params'][0]] ) ) {
				if ( ! isset( $count[$error['message']] ) ){
					$count[$error['message']] = [];
				}
				$count[$error['message']][$error['params'][0]] = 1;
			} else {
				$count[$error['message']][$error['params'][0]] = $count[$error['message']][$error['params'][0]] + 1;
			}
		}
		var_dump( $args );
		var_dump( $errors );
	});

var_dump( $count );
