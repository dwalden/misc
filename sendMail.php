use Eris\Generator;

function returnType( $typeStr ) {
	$isArray = false;
	if ( str_contains( $typeStr, '[]' ) ) {
		$isArray = true;
		$typeStr = substr( $typeStr, 0, -2 );
	}
	switch( $typeStr ) {
	case 'int':
		$type = Generator\int();
		break;
	case 'float':
		$type = Generator\float();
		break;
	case 'bool':
		$type = Generator\bool();
		break;
	case 'string':
		$type = Generator\string();
		break;
	case 'array':
		$type = Generator\seq( Generator\string() );
		break;
	case 'null':
		$type = Generator\constant(null);
		break;
	default:
		$type = Generator\string();
	}
	if ( $isArray ) {
		$type = Generator\seq( $type );
	}
	return $type;
}

$eris = new Eris\Facade();

$ref = new ReflectionClass( 'MediaWiki\User\User' );
$conn = $ref->newInstanceWithoutConstructor();

$props = [];
foreach ( $ref->getProperties() as $property ) {
	if ( $property->isPublic() ) {
		$name = $property->getName();
		$comment = $property->getDocComment();
		preg_match( '/\@var ([a-z]*\[?\]?)/', $comment, $matches );
		$type = returnType( $matches[1] );
		$props[$name] = $type;
	}
}

$foo = [];
$method = $ref->getMethod( 'sendMail' );
foreach ( $method->getParameters() as $param ) {
	// if ( $params->hasType() ) {

	// } else {
	    preg_match( "/\@param ([a-zA-Z\[\]\|]*) .{$param->getName()}/", $method->getDocComment(), $matches );
		$types = explode( '|', $matches[1] );
		$gens = [];
		foreach ( $types as $type ) {
			$gens[] = returnType( $type );
		}
		$foo[] = Generator\oneOf( ...$gens );
	// }
}

$count = [];

$eris
	->minimumEvaluationRatio(0)
	->forAll( Generator\associative( $props ), $foo )
	->then( function( $args, $fun ) use ( $ref, $conn, &$count ) {
		foreach ( $args as $name => $property ) {
			// Could use ReflectionProperty::setValue()
			$conn->{$name} = $property;
		}
		$status = $conn->sendMail( ...$fun );
		$errors = $status->getErrors();
		foreach ( $errors as $error ) {
			if ( ! isset( $count[$error['message']][$error['params'][0]] ) ) {
				if ( ! isset( $count[$error['message']] ) ){
					$count[$error['message']] = [];
				}
				$count[$error['message']][$error['params'][0]] = 1;
			} else {
				$count[$error['message']][$error['params'][0]] = $count[$error['message']][$error['params'][0]] + 1;
			}
		}
		var_dump( $args );
		var_dump( $errors );
	});

var_dump( $count );
